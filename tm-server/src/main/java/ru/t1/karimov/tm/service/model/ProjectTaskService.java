package ru.t1.karimov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.karimov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.karimov.tm.api.repository.model.IProjectRepository;
import ru.t1.karimov.tm.api.repository.model.ITaskRepository;
import ru.t1.karimov.tm.api.service.IConnectionService;
import ru.t1.karimov.tm.api.service.model.IProjectTaskService;
import ru.t1.karimov.tm.dto.model.TaskDto;
import ru.t1.karimov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.karimov.tm.exception.entity.TaskNotFoundException;
import ru.t1.karimov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.karimov.tm.exception.field.TaskIdEmptyException;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.repository.dto.ProjectDtoRepository;
import ru.t1.karimov.tm.repository.dto.TaskDtoRepository;
import ru.t1.karimov.tm.repository.model.ProjectRepository;
import ru.t1.karimov.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private ITaskRepository getTaskRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepository(entityManager);
    }

    @NotNull
    private IProjectRepository getProjectRepository(@NotNull final EntityManager entityManager) {
        return new ProjectRepository(entityManager);
    }

    @NotNull
    @Override
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(entityManager);
            @NotNull final ITaskRepository taskRepository = getTaskRepository(entityManager);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @Nullable final Task task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw  new TaskNotFoundException();
            task.setProject(projectRepository.findOneById(projectId));
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(entityManager);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            entityManager.getTransaction().begin();
            projectRepository.removeOneById(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);;
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @Nullable final TaskDto task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw  new TaskNotFoundException();
            task.setProjectId(null);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
