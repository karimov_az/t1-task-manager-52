package ru.t1.karimov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.UserDto;
import ru.t1.karimov.tm.enumerated.Role;

public interface IUserDtoRepository extends IDtoRepository<UserDto> {

    @NotNull
    UserDto create(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull
    UserDto create(
            @NotNull String login,
            @NotNull String password,
            @Nullable String email
    ) throws Exception;

    @NotNull
    UserDto create(
            @NotNull String login,
            @NotNull String password,
            @Nullable Role role
    ) throws Exception;

    @Nullable
    UserDto findByLogin(@NotNull String login) throws Exception;

    @Nullable
    UserDto findByEmail(@NotNull String email) throws Exception;

    boolean isLoginExist(@NotNull String login) throws Exception;

    boolean isEmailExist(@NotNull String email) throws Exception;

    @Override
    void removeAll() throws Exception;

    @Override
    void removeOne(@NotNull UserDto user) throws Exception;

}
