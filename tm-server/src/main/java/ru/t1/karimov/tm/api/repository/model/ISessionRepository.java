package ru.t1.karimov.tm.api.repository.model;

import ru.t1.karimov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
