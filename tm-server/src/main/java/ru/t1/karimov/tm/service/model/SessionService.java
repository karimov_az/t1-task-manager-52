package ru.t1.karimov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.repository.model.ISessionRepository;
import ru.t1.karimov.tm.api.service.IConnectionService;
import ru.t1.karimov.tm.api.service.model.ISessionService;
import ru.t1.karimov.tm.model.Session;
import ru.t1.karimov.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;

public final class SessionService extends AbstractUserOwnedService<Session> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ISessionRepository getRepository(@NotNull EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

}
